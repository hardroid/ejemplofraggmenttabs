package com.example.duoc.ciclodevida;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * A placeholder fragment containing a simple view.
 */
public class MenuPrincipalFragment extends Fragment {

    private Button btnIrADetalle;

    public MenuPrincipalFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_menu_principal, container, false);
        btnIrADetalle = (Button)v.findViewById(R.id.btnIrADetalle);
        btnIrADetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Es un click de Fragment", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

  /*  private void cambiarFragment() {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.contentPanel, new DetalleFragment(), "DetalleFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }*/
}
